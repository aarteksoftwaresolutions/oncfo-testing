/************* CREATED BY KOSHAL***************/
package Helpers;

import oncfo.Pages.BasePage;

import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;

public class Helper extends BasePage{
 
  @AfterMethod
  public void closeSetup(ITestResult result)
	{
		if(ITestResult.FAILURE==result.getStatus())
		{
			try{  
				captureScreenshot(result);
			} 
	        catch (Exception e){	 
	        	System.out.println("Exception while taking screenshot "+e.getMessage());
	         } 
		}
		System.out.println("Inside CloseSetup");
		closeSession();
		System.out.println("CloseSetup Done");
	
	}
  

}
