/************* CREATED BY KOSHAL***************/
package Helpers;

import org.testng.Assert;

public class oncfoAsserts {

	
	public void assertTrue(boolean condition){
		Assert.assertTrue(condition);
	}
	public void assertFalse(boolean condition){
		Assert.assertFalse(condition);
	}
	
	public void assertEquals(String actual, String expected){
		Assert.assertEquals(actual, expected);
	}
	
	public void assertNotEquals(String actual, String expected){
		Assert.assertNotEquals(actual, expected);
	}
	
}
