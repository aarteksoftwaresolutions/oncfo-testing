/************* CREATED BY KOSHAL***************/
package RegressionCheck;

import java.io.IOException;


import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Helpers.Helper;
import oncfo.Pages.LoginPage;

public class LoginRegressionTest extends Helper {
 

	@BeforeMethod
	public void InitialSetUp(){
		System.out.println("Inside Initial setup");
		InitialConfig();
		System.out.println("Initial setup Done");
	}
	
	@Test
	public void verifyInvalidLoginCredentialTest() throws InterruptedException, IOException{
		        System.out.println("Inside verify Invalid Login Credential Test : Trying to Login with invalid login credentials");
				LoginPage.login("praveen@gmail.com", "1234");
				waitForPageLoad(10);
				assertTrue(LoginPage.isInvalidEmailPasswordValidationPresent());
				System.out.println("Login Credentials are invalid");		
		
	}
	
	@Test
	public void verifyBlankLoginCredentialTest() throws InterruptedException, IOException{
		        System.out.println("Inside verify Blank Login Credential Test : Trying to Login with null login credentials");
				LoginPage.login("", "");
				waitForPageLoad(10);
				assertTrue(LoginPage.isemailIsMustValidationPresent());
				assertTrue(LoginPage.isPasswordIsMustValidationPresent());
				System.out.println("Login Credentials are null");		
		
	}
	
	@Test
	public void verifyBlankPasswordLoginCredentialTest() throws InterruptedException, IOException{
		        System.out.println("Inside verify Blank Password Login Credential Test : Trying to Login with null password login credentials");
				LoginPage.login("praveen@gmail.com", "");
				waitForPageLoad(10);
				assertTrue(LoginPage.isPasswordIsMustValidationPresent());
				System.out.println("Password is Must");		
		
	}

}
