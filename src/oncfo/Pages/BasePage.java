/************* CREATED BY KOSHAL***************/

package oncfo.Pages;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.io.File;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.swing.Action;

import org.apache.commons.io.FileUtils;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import org.testng.ITestResult;

import Helpers.oncfoAsserts;
import org.openqa.selenium.Capabilities;
import org.openqa.selenium.remote.RemoteWebDriver;


public class BasePage extends oncfoAsserts{
	
	 public static WebDriverWait wait;
	 public static WebDriver driver;

	 public void InitialConfig() {
		 	System.setProperty("webdriver.gecko.driver",""); 
	        driver = new FirefoxDriver();
	        driver.get("http://test.oncfo.net/login");
	        driver.manage().window().maximize();
	        waitForPageLoad(5);
	    }	    

     public static By findBy(String sLocator)
	    {
	        By byElement = null;
	        if (sLocator.contains("/"))
	        {
	            byElement = By.xpath(sLocator);
	        }
	        else{
	        	   byElement= By.id(sLocator);
	       
	        }
	        return byElement;    
	 }	          
 
	
     public static WebElement findObject(By ele, String selectorName )
     {      WebElement	rClientElement = null;  
     try{
    	 	rClientElement = driver.findElement(ele);
    	 	
         } 
         catch(Exception e){
        	 System.out.println ("ERROR: Element "+selectorName+"  not found");
         }
        
     return rClientElement;
    }
     
     
  
     public static List<WebElement> findElements(By ele, String selector)
     {

         List<WebElement> lsWebElement;
         lsWebElement = driver.findElements(ele);
         if (lsWebElement.size()==0)
             System.out.println("ERROR: Expected "+selector+" found No element ");
        return lsWebElement;

     }

     public static void navigateBack()
     {
         driver.navigate().back();
     }
     

     public static void setText(By ele, String selector ,String sText)
     {	
         WebElement wEle = findObject(ele, selector);
         wEle.sendKeys(sText);
     }
     
     public static void setSelectBoxText(By ele, String selectorName ,String sText)
     {	
         WebElement wEle = findObject(ele, selectorName);
         Select sel = new Select(wEle);
         sel.deselectByVisibleText(sText);
     }
     
     
     public static boolean isElementPresent(By ele, String selector)
     {	
    	 boolean status= false;
         WebElement wEle = findObject(ele, selector);
         
        try{
        status=wEle.isDisplayed();
        return status;
        }catch(Exception e){
        	 System.out.println("ERROR: Element "+selector+" is not present on screen ");
        }
        return status;
     }

     public static boolean isElementEnabled(By ele, String selector)
     {	
    	 boolean status= false;
         WebElement wEle = findObject(ele, selector);
         
        try{
        status=wEle.isEnabled();
        return status;
        }catch(Exception e){
        	 System.out.println("ERROR: Element "+selector+" is not Enabled on screen ");
        }
        return status;
     }
     
     
     public static void scrollPage(By ele, String selector){
    	 WebElement wEle = findObject(ele, selector);
    	 
    	((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", wEle);
     }
     
     
     public static void clickElement(By ele, String selector)
     {
         WebElement wEle = findObject(ele, selector);
        try {
            wEle.click();
        }catch (Exception e)
        {
        	 System.out.println("ERROR: Element "+selector+" is unclickable ");
        }

     }
     
     public static void clickActionsElement(By ele, String selector)
     {
         WebElement wEle = findObject(ele, selector);
        try {
           Actions act = new Actions(driver);
           act.click(wEle).build().perform();
        }catch (Exception e)
        {
        	 System.out.println("ERROR: Element "+selector+" is unclickable ");
        }

     }
     public static void DragAndDrop(By ele1,String sourceSelect,By ele2,String Destination){
    	 WebElement src= findObject(ele1,sourceSelect);
    	 WebElement des= findObject(ele1,Destination);
    	 try{
    		 Actions act = new Actions(driver);
    		 act.dragAndDrop(src, des).build().perform();
    	 }catch(Exception e){
    		 System.out.println("drag and drop action not perform");
    	 }
     }
     
     
    public void closeSession() {
         driver.quit();
     }
    public static void waitForPageLoad(int iTimeUnit){
    	 driver.manage().timeouts().implicitlyWait(iTimeUnit, TimeUnit.SECONDS);
    	 
     }
     
     
     public static String getCurrentPageTitle(){
    	 return driver.getTitle();
    	 
     }
     
     public static void sleepApplication(int iTimeUnit){
    	 
    	 try {
			Thread.sleep(iTimeUnit);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
     }
     
	    
     public static void captureScreenshot(ITestResult result) throws IOException {

    	 TakesScreenshot ts=(TakesScreenshot)driver;
			File source=ts.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(source, new File("./ScreenCapture/"+result.getName()+".png"));
     }
     
     
     public static void uploadFile(String filePath) throws AWTException{
    	    StringSelection ss = new StringSelection(filePath);
    	    Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
    	   
    	    //imitate mouse events like ENTER, CTRL+C, CTRL+V
    	    Robot robot = new Robot();
    	    robot.delay(250);
    	    	    
    	    robot.keyPress(KeyEvent.VK_ENTER);
    	  
    	    robot.keyRelease(KeyEvent.VK_ENTER);
    	
    	    robot.keyPress(KeyEvent.VK_CONTROL);
    	
    	    robot.keyPress(KeyEvent.VK_V);
    	
    	    robot.keyRelease(KeyEvent.VK_V);
    	    
    	    robot.keyRelease(KeyEvent.VK_CONTROL);
    	   
    	    robot.keyPress(KeyEvent.VK_ENTER);
  
    	    robot.keyRelease(KeyEvent.VK_ENTER);
    
     }
  
     
     
     
  }
	 


