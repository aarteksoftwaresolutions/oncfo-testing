/************* CREATED BY KOSHAL***************/
package oncfo.Pages;

import org.openqa.selenium.By;

public class LoginPage extends BasePage {
	
	public final static By byEmailFeild = findBy("//input[@id='email']"); 
	public final static By byPasswordFeild = findBy("//input[@id='password']"); 
	public final static By byLogInButton = findBy("//input[@value='Login']"); 
	public final static By byCreateAccountLink = findBy("//a[@id='register-btn']"); 
	public final static By byForgotPasswordLink= findBy("//a[@id='forget-password']");
	public final static By byRememberCheckBox= findBy("remember");
	public final static By byInvalidEmailPasswordValidation = findBy("//div[text()='Invalid Email and password']");
	public final static By byPasswordIsMustValidation = findBy("err_pass");
	public final static By byAccountConfirmationText = findBy("//div[contains(text(),'Your request has been sucessfully received')]");
	public final static By byemailIsMustValidation = findBy("//span[contans(text(),'Email is must')]");
	
	////////////////////////////////////////////////////////////
	//////////////////////  Validators  ////////////////////////
	///////////////////////////////////////////////////////////
	
	
	public static boolean isLoginPageLoaded(){
		return isElementPresent(byCreateAccountLink, "Create Account link");	
	}
	
	public static boolean isUserNamePresent(){
	return isElementPresent(byEmailFeild, "Email Feild");
	
	}
	
	public static boolean isPasswordFeildPresent(){
		return isElementPresent(byPasswordFeild, "Password Feild");
		
	}
	
	public static boolean isLoginInButtonPresent(){
		return isElementPresent(byLogInButton, "LogIn Button");
		
	}
	
	
	public static boolean isCreateAccountLinkPresent(){
		return isElementPresent(byCreateAccountLink, "Create Account link");
		
	}
	
	public static boolean isForgotPasswordLinkPresent(){
		return isElementPresent(byForgotPasswordLink, "Forgot password link");
		
	}
	
	
	public static boolean isInvalidEmailPasswordValidationPresent(){
		return isElementPresent(byInvalidEmailPasswordValidation, "Invalid Email Password Validation");
		
	}
	
	public static boolean isemailIsMustValidationPresent(){
		return isElementPresent(byemailIsMustValidation, "email Is Must Validation");
		
	}
	
	public static boolean isPasswordIsMustValidationPresent(){
		return isElementPresent(byPasswordIsMustValidation, "Password Is Must Validation");
		
	}
	
	
	
	public static boolean isAccountConfirmationTextPresent(){
		return isElementPresent(byAccountConfirmationText, "Account Confirmation Message");
	}
         ////////////////////////////////////////////////////////////
         //////////////////////  Setters ///////////////////////////
         ///////////////////////////////////////////////////////////
	
	
	public static void setEmailValue(String sText){
		setText(byEmailFeild,"Email Feild",sText);
		
	}
	
	public static void setPasswordValue(String sText){
		setText(byPasswordFeild, "Pssword Feild",sText);
	}	
	
	

	 ////////////////////////////////////////////////////////////
    //////////////////////  Clickers ///////////////////////////
    ///////////////////////////////////////////////////////////
	
	
	public static void clickLogIn(){
		
		clickElement(byLogInButton, "LogIn Button");

	}
	
	public static void clickCreateAccount(){
		
		clickElement(byCreateAccountLink, "Create Account link");

	}
	
	public static void clickForgotPassword(){
		
		clickElement(byForgotPasswordLink, "ForgotPasswrod  link");

	}
	
	

	
	 ////////////////////////////////////////////////////////////
    /////////////////////  Helper Methods /////////////////////
    ///////////////////////////////////////////////////////////
	
	public static void login(String sUserName, String sPassword){
		
		setEmailValue(sUserName);
		setPasswordValue(sPassword);
		clickLogIn();
	}
	
	
	
	
	
}
