package oncfo.Pages;

import org.openqa.selenium.By;

public class BuildPage extends BasePage {
	public final static By byBuildTab = findBy("//a[contains(text(),'Build')]"); 
	public final static By byCreateModuleLink = findBy("//span[contains(text(),'Create Module')]");
	public final static By byUploadingExcelLink = findBy("//a[contains(text(),'Uploading Excel')]");
	public final static By byModuleLibraryLink = findBy("//span[contains(text(),'Module Library')]");
	public final static By byFilterModule = findBy("moduleAreaSearch");
	public final static By byAssetsLink = findBy("//span[contains(text(),'Assets')]");
	public final static By byCapitalTab= findBy("//span[contains(text(),'Capital')]");
	public final static By byCapitalExpendiure		=findBy("//p[contains(text(),'Capital Expenditure')]");
	public final static By byViewChartText=findBy("//div[contains(text(),'View Chart')]");
	public final static By byPdfButton= findBy("//span[contains(text(),'PDF')]");
	public final static By byPrintButton= findBy("//span[contains(text(),Print)]");
	public final static By byQaTest2Tab=findBy("//span[contains(text(),'QATest2')]");
	//////////////create new Module/////////////////
	public final static By byUsingFrontendLink = findBy("//a[contains(text(),'Using Frontend')]");
	public final static By byCreatenewModule=findBy("//h1[contains(text(),'Create new Module')]");
	public final static By bySelectdataset=findBy("//*[@id='Cube']/div[1]/div[1]/div/select");
	public final static By byModuleName =findBy("//input[@class='form-control input-large module_name fl']");
	///////////validation/////////////////////////
	public static boolean isbuildElementPresent(){
		return isElementPresent(byBuildTab, "Build Tab");
	}
	public static boolean isAssetsElementPresent(){
		return isElementPresent(byAssetsLink,"By Assets Link");
	}
	public static boolean isCapitalExpediture(){
		return isElementPresent(byCapitalExpendiure,"by Capital Expendiure");	
	}
	public static boolean isQATest2tabPresent(){
		return isElementPresent(byQaTest2Tab, "by QATabTest"); 
	}
	public static boolean isCreatenewModulePresent(){
		return isElementPresent(byCreatenewModule,"by Create new Module field");
	}
	//////////////Setter////////////////////////////////////////////
	public static void Selectdatasetm(String sText){
		   setText(bySelectdataset,"By Select Dataset", sText);
	   }
	public static void SetModuleName(String sText){
		setText(byModuleName,"Module Name",sText);
	}
	/////////////clicker/////////
	public static void clickBuidTab(){
		clickElement(byBuildTab,"Build Tab");
	}
	public static void clickAssetsLink()
	{
	 clickElement(byAssetsLink,"By Assets link");
	}
	public static void clickCapitalExpendiure()
	{
	 clickElement(byCapitalExpendiure,"By CapitalExpendiure link");
	}
 	public static void clickViewChartText(){
 		clickElement(byViewChartText,"By View text");
 	}
 	public static void clickPdfButton(){
 		clickElement(byPdfButton,"By PDF Button");
 		
 	}
 	public static void clickPrintButton(){
 		clickElement(byPrintButton,"By PrintButton");
 	}
 	public static void clickUsingFrontedTab(){
 		clickElement(byUsingFrontendLink,"By UsingFrontendLink");
 	}
   public static void clickQATest2tab(){
	   clickElement(byQaTest2Tab,"By QaTest2Tab");
   }
   public static void clickSelectDataset(){
	   clickElement(bySelectdataset,"By Select Dataset");
   }
  public static void createnewModule(String Select){
   System.out.println("Inside New ModuleTab");
	   clickUsingFrontedTab();
	   System.out.println("clicked on Fronted tab");
	

   }
}
