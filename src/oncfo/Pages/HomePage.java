/************* CREATED BY KOSHAL***************/
package oncfo.Pages;

import org.openqa.selenium.By;

public class HomePage extends BasePage{
	
	
	/***************************** Home Tab Objects ****************************/
	public final static By byHomeTab = findBy("//a[contains(text(),'Home')]"); 
	public final static By byModelNavigation = findBy("//h3[contains(text(),'Model Navigation')]"); 
	public final static By byGlobalKPIsLink	 = findBy("//a[contains(text(),'Global KPIs')]"); 
	public final static By byRevenueForecastingLink = findBy("//a[contains(text(),'Revenue Forecasting')]"); 
	public final static By byRevenueWaterfallLink = findBy("//a[contains(text(),'Revenue Waterfall')]"); 
	//////////////
	public final static By byAssumptionTab = findBy("//span[contains(text(),'Assumptions')]");
	public final static By byVerticalText = findBy("//a[contains(text(),'Vertical Split')]");
	public final static By byCenterlizeTab= findBy("//a[contains(text(),'Centralized Assumptions')]");
	public final static By bySelectModule= findBy("//*[@id='bottom-screen']/div[2]/select");
	public final static By byFillRightLink=findBy("//a[contains(text(),'Fill Right')]");
	public final static By byHorizontalLink=findBy("//a[contains(text(),'Horizontal Split')]");
	public final static By bySaveScenarioTab=findBy("//a[contains(text(),'Save Scenario')]");
	/***************************** Settings Tab Objects ****************************/
	public final static By bySettingsTab = findBy("//a[contains(text(),'Settings')]"); 
	
	
	////////////////////////////////CAPITAL////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////////////////
	
	public final static By byCapitalTab= findBy("//span[contains(text(),'Capital')]");	
	
	
	/***************************** Build Tab Objects ****************************/
	public final static By byBuildTab = findBy("//a[contains(text(),'Build')]"); 
	public final static By byCreateModuleLink = findBy("//span[contains(text(),'Create Module')]");
	public final static By byUsingFrontendLink = findBy("//a[contains(text(),'Using Frontend')]");
	public final static By byUploadingExcelLink = findBy("//a[contains(text(),'Uploading Excel')]");
	public final static By byModuleLibraryLink = findBy("//span[contains(text(),'Module Library')]");
	public final static By byFilterModule = findBy("moduleAreaSearch");
	public final static By byAssetsLink = findBy("//span[contains(text(),'Assets')]");
		
    /****************************** Comman Objects ****************************/
	public final static By byFinancialStatementsLinks = findBy("//span[contains(text(),'Financial Statements')]"); 
	public final static By byOperationalLink = findBy("//span[contains(text(),'Operationals')]"); 
	public final static By byWorkingCapitalLinks = findBy("//span[contains(text(),'Working Capital')]"); 
	
	public final static By byTaxationLinks = findBy("//span[contains(text(),'Taxation')]"); 
	public final static By byCapitalLink = findBy("//span[contains(text(),'Capital')]");
	public final static By byChecksSensitivityLinks = findBy("//span[contains(text(),'Checks / Sensitivity')]");
	

	

	
	
	////////////////////////////////////////////////////////////
	//////////////////////  Validators  ////////////////////////
	///////////////////////////////////////////////////////////


	public static boolean isHomePageLoaded(){
		return isElementPresent(byHomeTab, "Home Tab");	
	}
	
	public static boolean isModelNavigationLinkOnScreen(){
		return isElementPresent(byModelNavigation,"Model Navigation Link");
	} 
	public static boolean isGlobalKPIsLinkOnScreen(){
		return isElementPresent(byGlobalKPIsLink,"Global KPIs Link");
	} 
	
	public static boolean isRevenueForecastingLinkOnScreen(){
		return isElementPresent(byRevenueForecastingLink,"Revenue Forecasting Link");
	} 
	
	public static boolean isRevenueWaterfallLinkOnScreen(){
		return isElementPresent(byRevenueWaterfallLink,"Revenue Waterfall Link");
	} 
	
	public static boolean isFinancialStatementsLinkOnScreen(){
		return isElementPresent(byFinancialStatementsLinks,"Financial Statements Link");
	} 
	
	public static boolean isCapitalLinkOnScreen(){
		return isElementPresent(byCapitalLink,"Capital Link");
	} 
	
	 
	
	public static boolean isWorkingCapitalLinksOnScreen(){
		return isElementPresent(byWorkingCapitalLinks,"Working Capital Link");
	} 
	
	public static boolean isTaxationLinkOnScreen(){
		return isElementPresent(byTaxationLinks,"Taxation Link");
	}
	
	public static boolean isChecksSensitivityOnScreen(){
		return isElementPresent(byChecksSensitivityLinks,"Checks Sensitivity Link");
	}
	
	public static boolean isHorizontalTab(){
		return isElementPresent(byHorizontalLink,"check Horizontal Tab");
	}
	public static boolean isFillRighLink(){
		return isElementPresent(byFillRightLink,"Check Fill Right Link");
	}
	public static boolean isSaveScenarioTab(){
		return isElementPresent(bySaveScenarioTab,"by Save Scenario Tab");
	}
	
////////////////////////////////////////////////////////////
//////////////////////  Clicker ////////////////////////
///////////////////////////////////////////////////////////
	
	
	public static void clickHomeTab(){
		clickElement(byHomeTab, "Home Tab");
	}
	
	public static void clickBuidTab(){
		clickElement(byBuildTab,"Build Tab");
	}
	
	public static void clickSettingsTab(){
		clickElement(bySettingsTab,"SettingsTab");
	}
	
	
	public static void clickUploadingExcelLink(){
		clickElement(byUploadingExcelLink,"Uploading Excel Link");
	}
/////////////////////////////////////////////////////////////////////////////////////
	public static void clickAssumptionTab(){
		clickElement(byAssumptionTab,"By Assumption Tab");
	}
	public static void clickCenterlizeTab(){
		clickElement(byCenterlizeTab, "By Centerlized Tab");
		
	}
	public static void clickVerticalTab(){
		clickElement(byVerticalText,"By vertical Text");
	}
	public static void clickSelectModuleTab(){
		clickElement(bySelectModule, "Select Module ");
		}
	///////////////////////////Capital Clicker/////////////////////
	public static void clickCapitalTab(){
		clickElement(byCapitalTab,"by capital Tab");
	}
	public static void clickHorizontalTab(){
		clickElement(byHorizontalLink,"By Horizontal Tab");
	}
	public static void clickFillRihtLink(){
		clickElement(byFillRightLink,"By Fill Right Link");
	}
	public static void clickSaveScenarioTab(){
		clickElement(bySaveScenarioTab,"By bySave ScenarioTab");
	}
	
	////////////////////////////////////////////////////////////////////////////////////////
	///////////////////////////SETTER//////////////////////////////////////////
	////////////////////////////////////////////////////////////////////////////
	public static void setSelectModule(String sText){
		setSelectBoxText(bySelectModule, "Select Module ",sText);
	}

	
	////////////////////////////////////////////////////
	//////////////////////////HELPER////////////////////////
	////////////////////////////////////////////////////////////
	public static void setSelectModulevalue(String bySelectModule){
		System.out.println("Insid Select Module tab");
		clickSelectModuleTab();
		System.out.println("pass Select Module tab");
		setSelectModule(bySelectModule);
		}
}
