/************* CREATED BY KOSHAL***************/
package oncfo.Pages;

import org.openqa.selenium.By;

public class CreateAccountPage extends BasePage {
	
    public final static By byAccountRequestText = findBy("//h3[text()='Account Request']");
	public final static By bySalutationSelectBox = findBy("//select[@id='salutation']"); 
	public final static By byFullNameFeild = findBy("name");
	public final static By byEmailFeild = findBy("email"); 
	public final static By byOrganizationNameFeild = findBy("register_organization_name"); 
	public final static By byWebSiteURL = findBy("register_website"); 
	public final static By byOrganizationSizeSelect = findBy("sizeOfOrganization"); 
	public final static By byNumberOfStaffSelect = findBy("numberOfStaff");
	public final static By byDesignationSelect = findBy("designation");
	public final static By byTC = findBy("//input[@type='checkbox']");
	public final static By byBackButton = findBy("register-back-btn");
	public final static By bySubmitButton = findBy("register-submit-btn");
	
	////////////////////////////////////////////////////////////
	//////////////////////  Validators  ////////////////////////
	///////////////////////////////////////////////////////////
	
	
	public static boolean isCreateAccountPageLoaded(){
		return isElementPresent(byAccountRequestText, "Account Request Text");	
	}
	
	public static boolean isSalutationSelectboxPresent(){
	return isElementPresent(bySalutationSelectBox, "Salutation Selectbox");
	
	}
	
	public static boolean isFullNameFeildPresent(){
		return isElementPresent(byFullNameFeild, "Full Name Feild");
		
	}
	
	public static boolean isEmailFeildPresent(){
		return isElementPresent(byEmailFeild, "Email Feild");
		
	}
	
	public static boolean isOrganizationNameFeildPresent(){
		return isElementPresent(byOrganizationNameFeild, "Organiation Name");
		
	}
	
	public static boolean isOrganizationSizeSelectPresent(){
		return isElementPresent(byOrganizationSizeSelect, "Organization Size SelectBox");
		
	}
	
	public static boolean isNumberOfStaffSelectPresent(){
		return isElementPresent(byNumberOfStaffSelect, "Number of staff SelectBox");
		
	}
	
	public static boolean isDesignationSelectPresent(){
		return isElementPresent(byDesignationSelect, "Designation Feild");
		
	}
	
	public static boolean isTCFeildPresent(){
		return isElementPresent(byTC, "T&C feild");
		
	}
	
	public static boolean isBackButtonPresent(){
		return isElementPresent(byBackButton, "Back Button");
		
	}
	
	public static boolean isSubmitButtonPresent(){
		return isElementPresent(bySubmitButton, "Submit Button");
		
	}
	public static boolean isWebSiteURLFeildPresent(){
		return isElementPresent(byWebSiteURL, "Web SiteURL Feild");
		
	}
	
	
	////////////////////////////////////////////////////////////
	//////////////////////  Setters ///////////////////////////
	///////////////////////////////////////////////////////////
	
	public static void setSalutationSelectboxValue(String sText){
		setSelectBoxText(bySalutationSelectBox,"Salutation select box",sText);
		
	}
	
	public static void setFullNameFeildValue(String sText){
		setText(byFullNameFeild, "Full Name Feild",sText);
	}
	
	
	public static void setEmailFeildValue(String sText){
		setText(byEmailFeild,"Email Feild",sText);
		
	}
	
	public static void setOrganizationNameFeildValue(String sText){
		setText(byOrganizationNameFeild, "Organization Name Feild",sText);
	}
	
	public static void setWebSiteURLFeildValue(String sText){
		setText(byWebSiteURL, "WebSite URL Feild",sText);
	}
	
	
	public static void setOrganizationSizeSelectBoxValue(String sText){
		setSelectBoxText(byOrganizationSizeSelect,"Organization Size select box",sText);
		
	}
	
	public static void setNumberOfStaffSelectBoxValue(String sText){
		setSelectBoxText(byNumberOfStaffSelect, "Number Of Staff Select box",sText);
	}
	
	
	public static void setDesignationSelectBoxValue(String sText){
		setSelectBoxText(byDesignationSelect,"Designation Select box",sText);
		
	}
	 ////////////////////////////////////////////////////////////
    //////////////////////  Clickers ///////////////////////////
    ///////////////////////////////////////////////////////////
	
	
	public static void clickTCFeild(){
		
		clickElement(byTC, "TCFeild");

	}
	
	public static void clickSubmitButton(){
		
		clickElement(bySubmitButton, "Submit Button");

	}
	
	public static void clickBackButton(){
		
		clickElement(byBackButton, "Back Button");

	}
	
	

	
	 ////////////////////////////////////////////////////////////
    /////////////////////  Helper Methods /////////////////////
    ///////////////////////////////////////////////////////////
	
	
	public static void CreateAccount(String salutation, String Name, String email, String organizationName, String url, String organizationSize, String staffCount , String designation){
		setSalutationSelectboxValue(salutation);
		setFullNameFeildValue(Name);
		setEmailFeildValue(email);
		setOrganizationNameFeildValue(organizationName);
		setWebSiteURLFeildValue(url);
		setOrganizationSizeSelectBoxValue(organizationSize);
		setNumberOfStaffSelectBoxValue(staffCount);
		setDesignationSelectBoxValue(designation);
		clickTCFeild();
		clickSubmitButton();
		
	}
	
}
