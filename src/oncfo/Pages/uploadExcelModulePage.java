package oncfo.Pages;
import java.awt.AWTException;
import org.openqa.selenium.By;

import com.thoughtworks.selenium.webdriven.commands.WaitForPageToLoad;

public class uploadExcelModulePage extends BasePage{
	public final static By byUploadExcelModuleTitle = findBy("//h1[contaisn(text(),'Upload Excel Module')]"); 
	public final static By bySelectFileButton = findBy("file"); 
	public final static By byChangeFileButton = findBy("//span[contains(text(),'Change')]"); 
	public final static By byRemoveFileButton = findBy("//a[contains(text(),'Remove')]"); 
	public final static By byFileTypeSwitch = findBy("//span[@class='bootstrap-switch-label']"); 
	public final static By byNextButton = findBy("//button[contains(text(),'Next')]"); 
	public final static By byResetButton= findBy("//button[contains(text(),'Reset')]");
	public final static By byFileNameFeild = findBy("//span[@class='fileinput-filename']");
	public final static By byProcessingBar = findBy("//div[@class='sub_overlay']/img");
	public final static By byModuleUploadSuccessMessage = findBy("//b[contains(text(),'Module(s) successfully uploaded;')]");

	/****************************** Module Library Objects ***************************/
	public final static By byPublicTab = findBy("//span[contains(text(),'Public')]"); 
	public final static By byPrivateTab = findBy("//a[contains(text(),'Private')]"); 
	public final static By byBrokenModulesTab = findBy("//a[contains(text(),'Broken Modules')]"); 
	public final static By byFilterTextBar = findBy("public"); 
	
	/****************************** Upload Excel Module Objects ***************************/
	public final static By byExcelModuleTable = findBy("//div[c@class='table-scrollable']"); 
	public final static By byIndividualModuleRadio = findBy("//label[1]/input"); 
	public final static By byModuleSuiteRadio = findBy("//label[2]/input"); 
	public final static By bySaveButton = findBy("//button[contains(text(),'Save')]"); 
	public final static By bySourceFile= findBy("//*[@id='1810']/div[1]");
	public final static By byAssetsLink = findBy("//span[contains(text(),'Assets')]");
	public final static By byRemoveButton= findBy("//a[contains(text(),'Remove')]");
	public final static By byDeletButton= findBy("//*[@id='1845']/div[1]/a[1]/i");
	public final static By byOkButton= findBy("//button[contains(text(),'OK')]");
	public final static By byEditdata = findBy("//a[@data-moduleid='1841']");
	public final static By byEditText=findBy("//iframe[@security='restricted']");
	public final static By bySubmitButton=findBy("//*[@id='modulesDescription']/div[2]/div/div/button[1]");
	////////////////////////////////////////////////////////////
	//////////////////////  Validators  ////////////////////////
	///////////////////////////////////////////////////////////


	public static boolean isUploadExcelPageLoaded(){
		return isElementPresent(byUploadExcelModuleTitle, "Upload Excel Module Title");	
	}

	public static boolean isSelectFileButtonPresent(){
		return isElementPresent(bySelectFileButton, "Select File Button");

	}

	public static boolean isExcelModuleTablePresent(){
		return isElementPresent(byExcelModuleTable, "Excel Module Table");

	}
	
	public static boolean isIndividualModuleRadioPresent(){
		return isElementPresent(byIndividualModuleRadio, "Individual Module Radio");

	}
	
	public static boolean isModuleSuiteRadioPresent(){
		return isElementPresent(byModuleSuiteRadio, "Module Suite Radio");

	}
	
	public static boolean isSaveButtonPresent(){
		return isElementPresent(bySaveButton, "Save Button");

	}

	public static boolean isChangeFileButtonPresent(){
		return isElementPresent(byChangeFileButton, "Change File Button");

	}

	public static boolean isRemoveFileButtonPresent(){
		return isElementPresent(byRemoveFileButton, "Remove File Button");

	}


	public static boolean isFileTypeSwitchPresent(){
		return isElementPresent(byFileTypeSwitch, "File Type Switch");

	}
	
	public static boolean isNextButtonPresent(){
		return isElementPresent(byNextButton, "Next Button");

	}

	public static boolean isResetButtonPresent(){
		return isElementPresent(byResetButton, "Reset Button");

	}

	public static boolean isFileNameFeildPresent(){
		return isElementPresent(byFileNameFeild, "File Name Feild");

	}
	public static boolean isPrivateTabPresent(){
		return isElementPresent(byPrivateTab, "Private Tab");

	}

	public static boolean isPublicTabPresent(){
		return isElementPresent(byPublicTab, "Public Tab");

	}

	public static boolean isBrokenModulesTabPresent(){
		return isElementPresent(byBrokenModulesTab, "Broken Modules Tab");

	}
	public static boolean isModuleUploadSuccessMessagePresent(){
		return isElementPresent(byModuleUploadSuccessMessage, "Module Upload Success Message");

	}

	public static boolean isFilterTextBarPresent(){
		return isElementPresent(byFilterTextBar, "Filter Text Bar");

	}
	public static boolean isProcessingBarPresent(){
		return isElementPresent(byProcessingBar, "Processing Bar");

	}
		/////////////////////////////////////////////////////
    //////////////////////  Clickers ///////////////////////////
    ///////////////////////////////////////////////////////////
	public static void clickOkButton(){
		clickElement(byOkButton,"click on OK");
	}
	public static void clickDeletButton(){
		clickElement(byDeletButton, "byDeletButton");
	}
	
	public static void clickSelectFileButton(){
		
		clickElement(bySelectFileButton, "SelectFile Button");

	}
	
	public static void clickChangeFileButton(){
		
		clickActionsElement(byChangeFileButton, "Create Account link");

	}
	
	public static void clickRemoveFileButton(){
		
		clickElement(byRemoveFileButton, "ForgotPasswrod  link");

	}
	
	public static void clickResetButton(){
		
		clickElement(byResetButton, "LogIn Button");

	}
	
	public static void clickFileTypeSwitch(){
		
		clickElement(byFileTypeSwitch, "Create Account link");

	}
	
	public static void clickNextButton(){
		
		clickElement(byNextButton, "ForgotPasswrod  link");

	}
	public static void clickPublicTab(){
		
		clickElement(byPublicTab, "Public Tab");

	}
	
	public static void clickPrivateTab(){
		
		clickElement(byPrivateTab, "Private Tab");

	}
	
	public static void clickBrokenModulesTab(){
		
		clickElement(byBrokenModulesTab, "Broken Modules Tab");

	}
	
	public static void clickFilterTextBar(){
		
		clickElement(byFilterTextBar, "Filter Text Bar");

	}
    public static void clickIndividualModuleRadio(){
		
		clickElement(byIndividualModuleRadio, "Individual Module Radio");

	}
	
	public static void clickModuleSuiteRadio(){
		
		clickElement(byModuleSuiteRadio, "Module Suite Radio");

	}
	
	public static void clickSaveButton(){
		
		clickElement(bySaveButton, "Save Button");

	}
	public static void clickEditButton(){
		clickElement(byEditdata,"by edit button");
	}
	public static void setEditText(String sText){
		setText(byEditdata,"Edit text Field",sText);
	}
	public static void clickSubmitButton(){
		clickElement(bySubmitButton,"clik on Submitbutton");
	}
			
	
	 ////////////////////////////////////////////////////////////
    //////////////////////  Helper ///////////////////////////
    ///////////////////////////////////////////////////////////
	
	
	public static void uploadExcel(String excelPath,String restricted) throws AWTException{
		
		clickSelectFileButton();
		System.out.println("clicked on selectFile");
		 sleepApplication(3000);
		uploadFile(excelPath);
		sleepApplication(2000);
		System.out.println("File Upload Done");
	
		clickPublicTab();
		System.out.println("fileupload done by Private");
	    waitForPageLoad(10);
		//clickChangeFileButton();
	//	sleepApplication(2000);
		System.out.println("ChangeButton File working");
		clickNextButton();
		System.out.println("clicked on NextButton for Nextprocess");
		waitForPageLoad(10);
		clickSaveButton();
		System.out.println("File Save");
		waitForPageLoad(10);
		DragAndDrop(bySourceFile,"File Module",byAssetsLink,null);
		System.out.println("process run drag");
		sleepApplication(2000);
		clickDeletButton();
		sleepApplication(2000);
		System.out.println("clicked on delete Button");
		clickOkButton();
		sleepApplication(2000);
		System.out.println("clicked on OK");
		clickEditButton();
		System.out.println("click on edit ");
		setEditText(restricted);
		sleepApplication(1000);
		System.out.println("text edited");
		clickSubmitButton();
		System.out.println("click on submit Button");
		
		
	}

}
