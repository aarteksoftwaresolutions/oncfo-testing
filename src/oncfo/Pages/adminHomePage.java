/************* CREATED BY KOSHAL***************/
package oncfo.Pages;

import org.openqa.selenium.By;

public class adminHomePage extends BasePage{
	public final static By byAdminText = findBy("//a[text()='Admin']"); 
	public final static By byModelNavigation = findBy("//h3[text(0='Model Navigation']"); 
	public final static By byDashboardsTab = findBy("//span[text()='Dashboards']"); 
	public final static By byAdministrationTab = findBy("//i[text()='Administration']"); 
	public final static By byNotificationBell = findBy("//i[@class='icon-bell']");
	public final static By byInboxIcon = findBy("//i[@class='icon-envelope-open']");

	
////////////////////////////////////////////////////////////
//////////////////////  Validators  ////////////////////////
///////////////////////////////////////////////////////////


public static boolean isAdminHomePageLoaded(){
return isElementPresent(byAdminText, "Admin Text");	
}

public static boolean isModelNavigationPresent(){
return isElementPresent(byModelNavigation, "Model Navigation");

}

public static boolean isDashboardTabPresent(){
return isElementPresent(byDashboardsTab, "Dashboards");

}

public static boolean isAdministrationTabPresent(){
return isElementPresent(byAdministrationTab, "Administration");

}

public static boolean isNotificationBellPresent(){
return isElementPresent(byNotificationBell, "Notification Bell");

}

public static boolean isInboxIconPresent(){
return isElementPresent(byInboxIcon, "Inbox Icon");

}
//////////////////////   //////////////////////////////////////////////
///////////////////Clicker/////////////////////////////////////////    
//////////////////////////////////////////////////////////////////////
	public static void ClickOnAdmicText(){
		clickElement(byAdminText,"click onAminText");
	}
	
}
