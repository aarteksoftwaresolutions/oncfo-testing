package SanityCheck;

import java.awt.AWTException;
import java.io.IOException;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import Helpers.Helper;
import oncfo.Pages.HomePage;
import oncfo.Pages.LoginPage;
import oncfo.Pages.uploadExcelModulePage;

public class uploadExcelSanityTest extends Helper {
	@BeforeMethod
	public void InitialSetUp(){
		System.out.println("Inside Initial setup");
		InitialConfig();
		System.out.println("Initial setup Done");
	}
	
	
	@Test
	public void verifyExcelUploadTest() throws InterruptedException, IOException, AWTException{
		        System.out.println("Inside Excel Upload Test : Trying to Login and upload excel");
		        LoginPage.login("david.greenbaum@oncfo.com","123456");
		        waitForPageLoad(20);
		        assertTrue(HomePage.isHomePageLoaded());
		        System.out.println("Login successfully");	
		        HomePage.clickBuidTab();
		        waitForPageLoad(20);
		        HomePage.clickUploadingExcelLink();
		        waitForPageLoad(20);
		        uploadExcelModulePage.clickSelectFileButton();
		        waitForPageLoad(20);
		        
		    
//
//		         public static void getOsType(){
//		        	 Capabilities cap = ((RemoteWebDriver) driver).getCapabilities();
//		        	    String browserName = cap.getBrowserName().toLowerCase();
//		        	    System.out.println(browserName);
//		        	    String os = cap.getPlatform().toString();
//		        	    System.out.println(os);
//		        	    String v = cap.getVersion().toString();
//		        	    System.out.println(v);
		        	 
		           
		        uploadExcelModulePage.uploadExcel("G:\\work\\tables_v2.xlsx","i am able to edit text");
		        assertTrue(uploadExcelModulePage.isProcessingBarPresent());
				System.out.println("Uploading sheet");
				sleepApplication(50000);
				assertTrue(uploadExcelModulePage.isExcelModuleTablePresent());
				System.out.println("Module fetch successfully");
				uploadExcelModulePage.clickSaveButton();
			    assertTrue(uploadExcelModulePage.isProcessingBarPresent());
			    sleepApplication(50000);
			    assertTrue(uploadExcelModulePage.isModuleUploadSuccessMessagePresent());
				System.out.println("Module upload successfully");		
		
	}
	
	@AfterMethod
	public void CloseSetUp(){
		System.out.println("Inside close setup");
		closeSession();
		System.out.println("Initial close done");
	}
}
