package SanityCheck;
import java.awt.AWTException;
import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Helpers.Helper;
import oncfo.Pages.HomePage;
import oncfo.Pages.LoginPage;

public class HomePageSanityCheck extends Helper {

@BeforeMethod
public void InitialSetUp(){
	System.out.println("Inside Initial setup");
	InitialConfig();
	System.out.println("Initial setup Done");
}


@Test
public void verifyHomeTab() throws InterruptedException, IOException, AWTException{
	        System.out.println("Inside Excel Upload Test : Trying to Login and upload excel");
	        LoginPage.login("david.greenbaum@oncfo.com","123456");
	        waitForPageLoad(20);
	        assertTrue(HomePage.isHomePageLoaded());
	        System.out.println("Login successfully");
	        HomePage.clickHomeTab();
	        waitForPageLoad(20);
	        HomePage.clickAssumptionTab();
	        waitForPageLoad(20);
	        assertTrue(HomePage.isHomePageLoaded());
	        HomePage.clickCenterlizeTab();
	        waitForPageLoad(20);
	        assertTrue(HomePage.isHomePageLoaded());
	        HomePage.clickVerticalTab();
	        waitForPageLoad(20);
	       // HomePage.clickSelectModuleTab();
    System.out.println("Select Module Field");
	HomePage.setSelectModulevalue("Sales-AE Revenue & COGs-SaaS");
	waitForPageLoad(20);
	}
@Test()
public void verifyHomePageCapitalTab() throws InterruptedException, IOException, AWTException{
	        System.out.println("Inside Excel Upload Test : Trying to Login and upload excel");
	        LoginPage.login("david.greenbaum@oncfo.com","123456");
	        waitForPageLoad(20);
	        assertTrue(HomePage.isHomePageLoaded());
	        System.out.println("Login successfully");
	        HomePage.clickHomeTab();
	        waitForPageLoad(20);
	        HomePage.clickCapitalTab();
	        
}


@AfterMethod
public void CloseSetUp(){
	System.out.println("Inside close setup");
	closeSession();
	System.out.println("Initial close done");
}

}
