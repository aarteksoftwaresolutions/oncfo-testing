/************* CREATED BY KOSHAL***************/
package SanityCheck;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Helpers.Helper;
import oncfo.Pages.CreateAccountPage;
import oncfo.Pages.LoginPage;


public class CreateAccountSanityTest extends Helper{
	
	
	@BeforeMethod
	public void InitialSetUp(){
		System.out.println("Inside Initial setup");
		InitialConfig();
		System.out.println("Initial setup Done");
	}
	
	@Test
	public void verifyCreateAccountTest() throws InterruptedException, IOException{
		        System.out.println("Inside Create Accout  Test : Trying to Submit create Account form");
				CreateAccountPage.CreateAccount("Mr.", "Test User", "Testuser@oncfo.com", "zanjo", "zanjo.io","1-10","1-5", "Others");
				waitForPageLoad(10);
				assertTrue(LoginPage.isAccountConfirmationTextPresent());
				System.out.println("Create Account Form sent successfully");		
		
	}
	
	
	@AfterMethod
	public void CloseSetUp(){
		System.out.println("Inside close setup");
		closeSession();
		System.out.println("Initial close done");
	}
	
  
}
