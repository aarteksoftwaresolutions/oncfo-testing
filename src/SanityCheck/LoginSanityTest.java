package SanityCheck;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Helpers.Helper;
import oncfo.Pages.HomePage;
import oncfo.Pages.LoginPage;
import oncfo.Pages.adminHomePage;


public class LoginSanityTest extends Helper{
 
	
	@BeforeMethod
	public void InitialSetUp(){
		System.out.println("Inside Initial setup");
		InitialConfig();
	//	getOsType();
		System.out.println("Initial setup Done");
	}
	
	
	@Test
	public void verifyValidAdminLoginTest() throws InterruptedException, IOException{
		        System.out.println("Inside Login Test : Trying to Login");
				LoginPage.login("david.greenbaum@oncfo.com", "123456");
				waitForPageLoad(10);
				assertTrue(adminHomePage.isAdminHomePageLoaded());
				System.out.println("Login Credentials entered successfully");		
		
	}
	@Test
	public void verifyValidClientLoginTest() throws InterruptedException, IOException{
		        System.out.println("Inside Login Test : Trying to Login");
				LoginPage.login("praveen@oncfo.com", "123456");
				waitForPageLoad(10);
				assertTrue(HomePage.isHomePageLoaded());
				System.out.println("Login Credentials entered successfully");		
		
	}
	@AfterMethod
	public void CloseSetUp(){
		System.out.println("Inside close setup");
		closeSession();
		System.out.println("Initial close done");
	}

	
}
