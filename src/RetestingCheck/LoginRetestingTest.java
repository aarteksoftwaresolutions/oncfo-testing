/************* CREATED BY KOSHAL***************/
package RetestingCheck;

import java.io.IOException;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import Helpers.Helper;

import oncfo.Pages.LoginPage;


public class LoginRetestingTest extends Helper{
	@BeforeMethod
	public void InitialSetUp(){
		System.out.println("Inside Initial setup");
		InitialConfig();
		System.out.println("Initial setup Done");
	}
	
	
	// BUG OC-524
	@Test
	public void verifyBlankLoginCredentialTest() throws InterruptedException, IOException{
		        System.out.println("Inside verify Blank Login Credential Test : Trying to Login with null login credentials");
				LoginPage.login("", "");
				waitForPageLoad(10);
				assertTrue(LoginPage.isemailIsMustValidationPresent());
				assertTrue(LoginPage.isPasswordIsMustValidationPresent());
				System.out.println("Login Credentials are null");		
		
	}
	
	// BUG OC-538
		@Test
		public void verifyLoginPageTitleTest() throws InterruptedException, IOException{
			        String expectedTitle= "onCFO";
					waitForPageLoad(10);
					assertEquals(getCurrentPageTitle(), expectedTitle);
					
					System.out.println("Login Page title varified");		
			
		}
	
	@AfterMethod
	public void CloseSetUp(){
		System.out.println("Inside close setup");
		closeSession();
		System.out.println("Initial close done");
	}
}
